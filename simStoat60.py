#!/usr/bin/env python

from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
from scipy.stats.mstats import mquantiles
import numpy as np
import pylab as P
import prettytable
from numba import autojit
import datetime

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

@autojit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat

def initialPStoatTrapCaptFX(params, basicdata, availTrapNights, location,
    g0Param, debug = False):      # prob that stoat was capt in trap
    distToTraps = basicdata.distTrapToCell2[:, location]
    eterm = np.exp(-(distToTraps) / params.var2)           # prob stoat-trap pair
    pNoCapt = 1. - g0Param * eterm
    pNoCaptNights = pNoCapt**(availTrapNights)
    pNoCaptNights = np.where(pNoCaptNights == 1., .9999, pNoCaptNights)
    return 1 - pNoCaptNights


def NpredInitialFX(nsession, N, removeDat, rpara, it, Npred):
    for i in range(nsession)[0:nsession-1]:
        Nday = N[i] - removeDat[i]
        Nday = np.where(Nday < 0, 0, Nday)
        Nday = np.multiply(rpara[i+1],Nday) + it[i+1]
        Npred[i+1] = Nday
    return(Npred)

def removeDatFX(nsession, stoat, session):
    removeDat = np.arange(nsession)
    for i in range(nsession):
        removeDat[i] = np.sum(stoat[session==i])
    return(removeDat)

        
def quantileFX(a):
    return mquantiles(a, prob=[0.025, 0.5, 0.975])

class Params(object):
    def __init__(self):
        self.iter = 1000
        self.nsess = 21 
        self.ncov = 3
        self.popThreshold = 11
        self.reduceImm = .25
        self.reduceRepro = 1.93    #2.56
        self.g0Increase = 2.0

class BasicData(object):
    def __init__(self, captFname, datesFname, trapFname, covFname, maxTrapNights = 7):

        self.maxTrapNights = maxTrapNights

        self.capt6 = np.genfromtxt(captFname,  delimiter=',', names=True,
            dtype=['f8', 'S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'S10', 'S10',
            'i8', 'i8', 'i8', 'S12', 'i8', 'i8', 'i8', 'f8', 'i8', 'f8', 'i8'])

        self.trap = np.genfromtxt(trapFname, delimiter=',', names=True,
            dtype=['S10', 'f8', 'f8', 'f8', 'f8'])

        self.stoat = self.capt6['stoat']
        self.session = self.capt6['session'] - 1
        self.nsession = np.int(max(self.session + 1))

        self.dates = np.genfromtxt(datesFname, delimiter=',', names=True,
            dtype=['S10', 'i8', 'S10', 'i8', 'i8', 'i8', 'S10', 'i8',
            'i8', 'i8', 'f8'])
        self.month = self.dates['mo']
        self.year = self.dates['year'] + 2000
        self.interval = self.dates['interval']
        self.intervalSession = np.where(self.interval < (self.maxTrapNights + 1),
        self.interval, self.maxTrapNights)

        def getTrapIDFX(self):
            self.ntrap = len(self.trap['sid'])
            trapSid = self.trap['sid']
            self.trapID = np.arange(self.ntrap)
            self.trapX = self.trap['easting']
            self.trapY = self.trap['northing']
            captSid = self.capt6['sid']
            nCaptDat = len(captSid)
            self.captTrapID = np.empty(nCaptDat, dtype = int)           # trap ID in capt6 data

            for i in range(nCaptDat):
                self.captTrapID[i] = self.trapID[captSid[i] == trapSid]

        getTrapIDFX(self)

        self.trapAvail = np.ones(self.ntrap)

        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.cellX = self.covDat['x1']
        self.cellY = self.covDat['x2']
        self.eastCov = self.covDat['x1'] - min(self.covDat['x1'])
        self.northCov = self.covDat['x2'] - min(self.covDat['x2'])
        self.nw = self.covDat['cellDevNW']
        self.ndev = self.covDat['cellDevN']
        self.terrIndx = self.covDat['terrIndx']
        self.ncell = len(self.eastCov)

#        self.cellSession = np.repeat(np.arange(0, 14, 1), self.ncell)
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleNW = (self.nw - np.mean(self.nw)) / np.std(self.nw)
        self.scaleDevN = (self.ndev - np.mean(self.ndev)) / np.std(self.ndev)
        self.scaleTerrIndx = (self.terrIndx - np.mean(self.terrIndx)) / np.std(self.terrIndx)

        self.scaleEast2 = (self.eastCov**2 - np.mean(self.eastCov**2)) / np.std(self.eastCov**2)
        self.scaleNorth2 = (self.northCov**2 - np.mean(self.northCov**2)) / np.std(self.northCov**2)

        self.xdat = np.hstack([np.expand_dims(np.ones(self.ncell),1), np.expand_dims(self.scaleEast,1),
            np.expand_dims(self.scaleNorth,1)])

        self.nbcov = np.shape(self.xdat)[1]

        self.removeDat = removeDatFX(self.nsession, self.stoat, self.session)
        self.cellID = np.arange(0, self.ncell, dtype = int)
        self.nStoatInCellTemplate = np.zeros(self.ncell)

        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0

class GibbsData(object):
    def __init__(self, gibbsFname, params):
        self.gibbsResults = np.loadtxt(gibbsFname)
        self.npara = np.shape(self.gibbsResults)[1]
        self.ngibbs = np.shape(self.gibbsResults)[0]
        self.nDataGibbs = self.gibbsResults[:, (params.ncov + 1) : (self.npara - 4)]
        self.rPool = self.gibbsResults[:, self.npara - 4]
        self.iPool = self.gibbsResults[:, self.npara - 3]
        self.g0Pool =  self.gibbsResults[:, self.npara - 2] * params.g0Increase
        self.sigmaPool = self.gibbsResults[:, self.npara - 1]

class Simul(object):
    def __init__(self, params, gibbsdata, basicdata):

        def nMatrixFX(self):
            self.nQuantileMat = np.zeros(shape=(3,(basicdata.nsession + params.nsess)))
            remDatMat = np.expand_dims(basicdata.removeDat,1)
            nGibbsTransposed = np.transpose(gibbsdata.nDataGibbs)
            postTrapNGibbs = nGibbsTransposed - remDatMat
            postTrapNGibbs2 = np.transpose(postTrapNGibbs)
            nQuantsGibbs = np.apply_along_axis(quantileFX, 0, postTrapNGibbs2)
            nMeanGibbs = np.apply_along_axis(np.mean, 0, postTrapNGibbs2)
            self.nQuantileMat[:, 0:basicdata.nsession] = nQuantsGibbs
            self.nQuantileMat[1, 0:basicdata.nsession] = nMeanGibbs
            self.nSimMat = np.zeros(shape=(params.iter, params.nsess))
            self.nPostTrapMat = np.zeros(shape=(params.iter, params.nsess))

            self.nPreQuantMat = np.zeros(shape=(3,(basicdata.nsession + params.nsess)))
            nPreQuantsGibbs = np.apply_along_axis(quantileFX, 0, gibbsdata.nDataGibbs)
            nPreMean =  np.apply_along_axis(np.mean, 0, gibbsdata.nDataGibbs)
            self.nPreQuantMat[:, 0:basicdata.nsession] = nPreQuantsGibbs
            self.nPreQuantMat[1, 0:basicdata.nsession] = nPreMean


        nMatrixFX(self)

        self.sampID = np.random.choice(range(gibbsdata.ngibbs), params.iter, replace = False)
        self.nMatPred = np.zeros(shape = (params.iter, params.nsess))
        self.months = np.tile(np.array([1,7,11]), 7)
        self.simYears = np.repeat(np.array([2013, 2014, 2015, 2016, 2017, 2018, 2019]), 3)
#        self.simYears = yearTemp[1:21]
#        self.months = monTemp[1:21]
        self.nNovember = len(self.months[self.months == 11])
        self.days = np.tile(np.array([75, 174, 120]), 7)
#        self.days = intervalTemp[1:21]
        self.reproPara = np.ones(params.nsess)
        self.nTrapRSel = round(basicdata.ntrap * 0.15)          #############
                                                                #######
                                                                ##
        self.nEradications = 0
        self.nSuppressions = 0

        def getDMatFX(basicdata, nPreTrap, thMultiNom):
            stoatsInCells = np.random.multinomial(nPreTrap, thMultiNom, size = None)
            nStoatsPres = stoatsInCells[stoatsInCells > 0]
            cellIDSess = basicdata.cellID[stoatsInCells>0]
            dMat = basicdata.distTrapToCell2[:, cellIDSess]
            return(dMat, stoatsInCells, cellIDSess, nStoatsPres)

        def getAvailTrapFX(self, basicdata):
            rSel = np.random.choice(basicdata.trapID, self.nTrapRSel, replace = False)
            availTrap = basicdata.trapAvail.copy()
            availTrap[rSel] = 0.5      #################################################################################
                                                    #####################################################################
                                                                #########################################################
            availTrapNights = np.expand_dims(availTrap * basicdata.maxTrapNights, 1)
            return availTrapNights

        def pCaptFX(self, basicdata, dMat, availTrapNights, sigmaIter, g0Iter):   
            eterm = np.exp(-(dMat) / sigmaIter**2)
            pNoCapt = 1.0 - g0Iter * eterm
            pNoCaptNights = pNoCapt**(availTrapNights)
            pNoCaptNights = np.where(pNoCaptNights == 1.0, 0.9999, pNoCaptNights)
            pNoCaptNightsTraps = pNoCaptNights.prod(axis = 0)
            return 1.0 - pNoCaptNightsTraps

        def simulFX(self, basicdata, gibbsdata, params):
            for i in range(params.iter):
                nStart = gibbsdata.nDataGibbs[self.sampID[i],15] - basicdata.removeDat[15]

                randRepro = np.random.choice(gibbsdata.rPool,
                    size = self.nNovember, replace = True)
                self.reproPara[self.months==11] = randRepro / params.reduceRepro

                pImm = np.random.choice(gibbsdata.iPool, size = params.nsess, replace = True)
                immPara = pImm * self.days / 365.0 * params.reduceImm
                self.b = np.expand_dims(gibbsdata.gibbsResults[self.sampID[i], 0 : params.ncov], 1)
                self.mu = np.dot(basicdata.xdat,self.b)
                thMultiNomTemp = thProbFX(self.mu, debug = False)
                thMultiNom = thMultiNomTemp.flatten()
                g0Iter = np.random.choice(gibbsdata.g0Pool, size = params.nsess,
                    replace = True)
                sigmaIter = np.random.choice(gibbsdata.sigmaPool, size = params.nsess,
                    replace = True)
                nNow = nStart.copy()

                for j in range(params.nsess):
                    lambdaPara = nNow * self.reproPara[j] + immPara[j]
                    nPreTrap = round(lambdaPara)
 
                    if nPreTrap == 0:
                        nNow = 0

                    if nPreTrap > 0:
                        (dMat, stoatsInCells, cellIDSess, nStoatsPres) = getDMatFX(basicdata, nPreTrap, thMultiNom)

                        availTrapNights = getAvailTrapFX(self, basicdata)

                        pCapt = pCaptFX(self, basicdata, dMat, availTrapNights, sigmaIter[j], g0Iter[j])

                        captStoats = np.random.binomial(nStoatsPres, pCapt, size = None)

                        nCapt = np.sum(captStoats)
                        nNow = nPreTrap - nCapt
                    self.nSimMat[i,j] = nPreTrap
                    self.nPostTrapMat[i,j] = nNow

                if np.sum(self.nPostTrapMat[i, (params.nsess - 15) : params.nsess]) == 0:
                    self.nEradications = self.nEradications + 1

                if np.max(self.nSimMat[i, (params.nsess - 15) : params.nsess]) < params.popThreshold:
                    self.nSuppressions = self.nSuppressions + 1

            probErad = self.nEradications / params.iter
            probSuppression = self.nSuppressions / params.iter
            preTrapQuants = np.apply_along_axis(quantileFX, 0, self.nSimMat)
            postTrapQuants = np.apply_along_axis(quantileFX, 0, self.nPostTrapMat)
            meanPreN = np.apply_along_axis(np.mean, 0, self.nSimMat)
            meanPostN = np.apply_along_axis(np.mean, 0, self.nPostTrapMat)

            matCols = np.shape(self.nQuantileMat)[1]
            self.nQuantileMat[:, (matCols - params.nsess) : matCols] = postTrapQuants
            self.nQuantileMat[1, (matCols - params.nsess) : matCols] = meanPostN

            self.nPreQuantMat[:, (matCols - params.nsess) : matCols] = preTrapQuants
            self.nPreQuantMat[1, (matCols - params.nsess) : matCols] = meanPreN

            print('probability of Erad', probErad)
            print('Probability of Suppression', probSuppression)
                    
        simulFX(self, basicdata, gibbsdata, params)


class ResultsProcessing(object):
    def __init__(self, basicdata, simobj):
               
        self.mo = np.append(basicdata.month, simobj.months)
        self.yr = np.append(basicdata.year, simobj.simYears)

    def makeTableFX(self, simobj):
        resultNPostTrap = simobj.nQuantileMat.transpose()
        resultNPostTrap = np.round(resultNPostTrap, 4)
            

        aa = prettytable.PrettyTable(['Months', 'Years', 'Low CI', 'Mean', 'High CI'])
        months = self.mo
        years = self.yr
        for i in range(np.shape(resultNPostTrap)[0]):
            month = months[i]
            year = years[i]
            row = [month] + [year] + resultNPostTrap[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryNPost = resultNPostTrap.copy()
           


    def writeToFileFX(self):
        (m, n) = self.summaryNPost.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Months', np.integer), ('Years', np.integer), ('Low CI', np.float),
                    ('Mean', np.float), ('High CI', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryNPost[:, 0]
        structured['Mean'] = self.summaryNPost[:, 1]
        structured['High CI'] = self.summaryNPost[:, 2]

        structured['Months'] = self.mo.astype(np.integer)
        structured['Years'] = self.yr.astype(np.integer)

#        np.savetxt('summarySim60A.txt', structured, fmt=['%d', '%d', '%.4f', '%.4f', '%.4f'],
#                    header='Months Years Low_CI Mean High_CI')

    def plotFX(self, simobj):
        dates = []
        minDate = datetime.date(2008, 5, 1)
        maxDate = datetime.date(2019, 12, 1)
        for month, year in zip(self.mo, self.yr):
            date = datetime.date(int(year), int(month), 1)
            dates.append(date)

        P.figure(0)
#        P.subplot(1,2,0)
        P.plot(dates, simobj.nQuantileMat[1,:], label = 'Mean post-trap pop.', color = 'k', linewidth = 3)
        P.plot(dates, simobj.nQuantileMat[2,:], label = '97.5th percentile', color = 'k')
        P.xlabel('Time', fontsize = 17)
        P.ylabel('Pop. size after trapping', fontsize = 17)
        P.legend(loc='upper right')
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate) 
        P.savefig("simPlot60N.eps", format='eps')
        P.savefig("simPlot60N.pdf", format='pdf')
        P.savefig("simPlot60N.png", format='png', dpi = 1000)

        P.figure(1)
#        P.subplot(1,2,1)
        P.plot(dates, simobj.nPreQuantMat[1, :], label = 'Mean pre-trap pop.', color = 'k', linewidth = 3)
        P.plot(dates, simobj.nPreQuantMat[2,:], label = '97.5th percentile', color = 'k')
        P.ylim([0, 140])
        P.xlabel('Time', fontsize = 17)
        P.ylabel('Pop. size prior trapping', fontsize = 17)
        P.legend(loc='upper right')
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate) 
#        P.savefig("simPlotPre60B.png", format='png', dpi = 1000)

        P.show()


########            Main function
#######
def main():

    #np.seterr(all='raise')

    params = Params()

    basicdata = BasicData('capt11.csv', 'datesBindSim.csv', 'traploc3.csv', 'covDat.csv')

    gibbsdata = GibbsData('mcmcResults_Rd_M14_R5_I4.txt', params)

    simobj = Simul(params, gibbsdata, basicdata)

    resultsobj = ResultsProcessing(basicdata, simobj)

    resultsobj.makeTableFX(simobj)

    resultsobj.writeToFileFX()

    resultsobj.plotFX(simobj)




if __name__ == '__main__':
    main()



