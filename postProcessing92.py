#!/usr/bin/env python

import os
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable

class ResultsProcessing(object):
    def __init__(self, resultsFname):
        """
        Class and functions to process mcmc results, and 
        produce summary table and diagnostic trace plots
        """
        self.resultsdata = np.loadtxt(resultsFname)
        self.ncov = np.shape(self.resultsdata)[1]
        self.ndat = np.shape(self.resultsdata)[0]

########################
########################
########################
        # path and data files of results to write to project directory
        self.stoatpath = os.getenv('STOATSPROJDIR', default='.')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_ressy92.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92Informed.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92Weak.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92VeryWeak.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92ReproWeak2.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92JeffreyWeak.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92JeffreyReproWeak.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92JeffreyImmWeak.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92JeffreyVague.txt')

#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M1.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M2.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M3.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M4.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M5.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M6.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M3_Sig.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M9.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M10.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M11.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M12.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M13.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M14.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M15.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M16.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M17.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M18.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M20.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M21.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M22.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M23.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M24.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M25.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M26.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M27.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M28.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M29.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M30.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M31.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M32.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M33.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M34.txt')
###        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M35.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M36.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M37.txt')
#        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M38.txt')
        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_92M39.txt')



########################
########################
########################


    @staticmethod    
    def quantileFX(a):
        """
        quantile function
        """
        return mquantiles(a, prob=[0.05, 0.5, 0.95])
    
    def makeTableFX(self):
        """
        make summary table of parameter estimates from mcmc
        """
        resultTable = np.zeros(shape = (4, 29))
        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.resultsdata)
        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.resultsdata)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'Imm_Jul', 'Imm_Nov', 'Imm_Jan', 'g0_Jul', 'g0_Nov', 'g0_Jan',
             'HR_Sigma', 'DevianceStat']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.summaryTable = np.round(resultTable.copy(), 4)
        #return resultTable

    def plotFX(self):
        """
        plot diagnostic trace plots
        """
        cc = 0
        nfigures = np.int(np.ceil(self.ncov/6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < self.ncov:
                    P.plot(self.resultsdata[0:self.ndat, cc])
                    P.title(cc)
                cc = cc + 1
            P.show(block=lastFigure)

########            Write data to file
########
    def writeToFileFX(self):
        """
        write to directory a table of summary statistics of parameters
        """
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'ImmJul', 'Imm_Nov', 'Imm_Jan', 'g0_Jul', 'g0_Nov', 
            'g0_Jan', 'HR_Sigma', 'DevianceStat']

        structured['Names'] = names

        np.savetxt(self.summaryFile, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')



def main():

    # path to project directory to read in data and write results
    stoatpath = os.getenv('STOATSPROJDIR', default='.')

    # paths and data to read in
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_ressy92.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92Informed.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92Weak.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92VeryWeak.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92ReproWeak2.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92JeffreyWeak.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92JeffreyReproWeak.txt')

#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92JeffreyImmWeak.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92JeffreyVague.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M1.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M2.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M3.txt')

#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M4.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M5.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M6.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M3_Sig.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M9.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M10.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M11.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M12.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M13.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M14.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M15.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M16.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M17.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M18.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M19.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M20.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M21.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M22.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M23.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M24.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M25.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M26.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M27.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M28.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M29.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M30.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M31.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M32.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M33.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M34.txt')
##    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M35.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M36.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M37.txt')
#    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M38.txt')
    resultDatFile = os.path.join(stoatpath,'mcmcResults_92M39.txt')




    # run ResultsProcessing and read in data
    resultsobj = ResultsProcessing(resultDatFile)
    
    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
    resultsobj.writeToFileFX()
   
if __name__ == '__main__':
    main()


