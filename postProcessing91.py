#!/usr/bin/env python

import os
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable

class ResultsProcessing(object):
    def __init__(self, resultsFname):
        """
        Class and functions to process mcmc results, and 
        produce summary table and diagnostic trace plots
        """
        self.resultsdata = np.loadtxt(resultsFname)
        self.ncov = np.shape(self.resultsdata)[1]
        self.ndat = np.shape(self.resultsdata)[0]

########################
########################
########################
        # path and data files of results to write to project directory
        self.stoatpath = os.getenv('STOATSPROJDIR', default='.')
        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_ressy91.txt')
########################
########################
########################


    @staticmethod    
    def quantileFX(a):
        """
        quantile function
        """
        return mquantiles(a, prob=[0.05, 0.5, 0.95])
    
    def makeTableFX(self):
        """
        make summary table of parameter estimates from mcmc
        """
        resultTable = np.zeros(shape = (4, 26))
        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.resultsdata)
        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.resultsdata)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'Imm_Nov', 'Imm_Jan', 'Imm_Jul', 'g0', 'HR_Sigma']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.summaryTable = np.round(resultTable.copy(), 4)
        #return resultTable

    def plotFX(self):
        """
        plot diagnostic trace plots
        """
        cc = 0
        nfigures = np.int(np.ceil(self.ncov/6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < self.ncov:
                    P.plot(self.resultsdata[0:self.ndat, cc])
                    P.title(cc)
                cc = cc + 1
            P.show(block=lastFigure)

########            Write data to file
########
    def writeToFileFX(self):
        """
        write to directory a table of summary statistics of parameters
        """
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'Imm_Nov', 'Imm_Jan', 'Imm_Jul', 'g0', 'HR_Sigma']

        structured['Names'] = names

        np.savetxt(self.summaryFile, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')


def main():

    # path to project directory to read in data and write results
    stoatpath = os.getenv('STOATSPROJDIR', default='.')

    # paths and data to read in
    resultDatFile = os.path.join(stoatpath,'mcmcResults_ressy91.txt')
 
    # run ResultsProcessing and read in data
    resultsobj = ResultsProcessing(resultDatFile)
    
    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
    resultsobj.writeToFileFX()
   
if __name__ == '__main__':
    main()


