#!/usr/bin/env python


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable

class ResultsProcessing(object):
    def __init__(self, resultsFname):
        """
        Class and functions to print a table of summary statistics to console 
        and produce parameter trace plots 
        """
        self.resultsdata = np.loadtxt(resultsFname)
        self.ncov = np.shape(self.resultsdata)[1]
        self.ndat = np.shape(self.resultsdata)[0]

    @staticmethod    
    def quantileFX(a):
        """
        quantiles
        """
        return mquantiles(a, prob=[0.025, 0.5, 0.975])
    
    def makeTableFX(self):
        resultTable = np.apply_along_axis(self.quantileFX, 0, self.resultsdata)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI'])

        names = ['Intercept', 'Eastings', 'Northings', 'NW Dev', 'Var', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'Repro', 
            'Immigration', 'g0']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)

        self.summaryTable = np.round(resultTable.copy(), 4)

    def plotFX(self):
        cc = 0
        nfigures = np.int(np.ceil(self.ncov/6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < self.ncov:
                    P.plot(self.resultsdata[0:self.ndat, cc])
                    P.title(cc)
                cc = cc + 1
            P.show(block=lastFigure)

########            Write data to file
########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        names = ['Intercept', 'Eastings', 'Northings', 'N_Dev', 'Var', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'Repro', 
            'Immigration', 'g0']

        structured['Names'] = names

        np.savetxt('summaryTable_try.txt', structured, fmt=['%s', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI')


def main():

    resultsobj = ResultsProcessing('mcmcResults_try.txt')
    
    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
    resultsobj.writeToFileFX()
   
if __name__ == '__main__':
    main()


