#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
#import starlings
#import params
from scipy.special import gamma
from scipy.special import gammaln
#from starlings import dwrpcauchy

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


class G0Test(object):
    def __init__(self):

        self.gg = np.arange(.001,.99, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        mode = .04
        a = 1.3
#        b = 4
        b = ((a-1.0)/mode) - a + 2.0
        print("a and b", a, b)
        print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
        print('mode', mode) 
#        randbeta = np.random.beta(a,b,100000)
#        print('nabove', len(randbeta[randbeta>.04]), 'nbelow', len(randbeta[randbeta < .04]))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
#        P.ylim(0., 15.0)
        P.xlim(0.001, .99)        
#        P.hist(randbeta)
        P.show()
        P.cla()

    @staticmethod
    def getBetaFX(mg0, g0Sd):
        a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        return (a, b)

class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(.01, 1.0, .001)
        mode = 1.0
        sh = .01
        sc = .01    #.1    # 1.0/ 5.4  #2.25   #1.6667  #3.    
#        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
#        sc = 3.0     # 1.5     # .66667    
#        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
        randg = np.random.gamma(sh, sc, size = 2000)          # use shape and scale
        print('max randg', np.max(randg))
        print("mean", sh*sc, "sd", np.sqrt(sh*sc**2), "mode", (sh-1.)*sc)
        print("parameter calc mean", sh * sc)
        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure()
        P.subplot(1,2,1)
        P.hist(randg)
        P.subplot(1,2,2)
        P.plot(self.rr, self.dpdf)
        P.ylim(0, .03)
        P.show()
        P.cla()

class wrpCTest(object):
    def __init__(self):
        days = np.arange(365)
        aa = days/364 * 2. 
        aa = np.arange(0, (np.pi*2), .05)
        mu = 1.4    # np.pi*2
        rho = 3.   #.7
        dwrpc = dwrpcauchy(aa, mu, rho)
        st_dwrpc = (dwrpc/np.max(dwrpc)) * 0.05
        print(dwrpc)
        print('st_dwrpc', st_dwrpc)
        P.figure(0)
#        P.plot(aa, dwrpc)
        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()
        P.cla()
        

class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)






########            Main function
#######
def main():

#    G0Test()
    gammaTest()
#    wrpCTest()
#    normalBeta()
#    Poisson()

if __name__ == '__main__':
#    PP = params.Params()
    main()

