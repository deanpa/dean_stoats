#!/bin/bash
#SBATCH -J M11_30thin
#SBATCH -A landcare00026
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=71:55:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

#srun startStoats.py

srun startStoats.py --basicdata=$STOATSPROJDIR/out_basicdata_M11.pkl --params=$STOATSPROJDIR/out_params_M11.pkl --stoatdata=$STOATSPROJDIR/out_stoatdata_M11.pkl
