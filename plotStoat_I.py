#!/usr/bin/env python

from scipy import stats
from scipy.stats.mstats import mquantiles
import numpy as np
import pylab as P
import prettytable
import datetime
from matplotlib.ticker import MaxNLocator
from osgeo import gdal

def removeDatFX(nsession, stoat, session):
    removeDat = np.arange(nsession)
    for i in range(nsession):
        removeDat[i] = np.sum(stoat[session==i])
    return(removeDat)


def quantileFX(a):
    return mquantiles(a, prob=[0.025, 0.5, 0.975])

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

class BasicData(object):
    def __init__(self, captFname, datesFname, trapFname, covFname, maxTrapNights = 7):

        self.maxTrapNights = maxTrapNights

        self.capt6 = np.genfromtxt(captFname,  delimiter=',', names=True,
            dtype=['f8', 'S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'S10', 'S10',
            'i8', 'i8', 'i8', 'S12', 'i8', 'i8', 'i8', 'f8', 'i8', 'f8', 'i8'])

        self.trap = np.genfromtxt(trapFname, delimiter=',', names=True,
            dtype=['S10', 'f8', 'f8', 'f8', 'f8'])

        self.stoat = self.capt6['stoat']
        self.session = self.capt6['session'] - 1
        self.nsession = np.int(max(self.session + 1))

        self.dates = np.genfromtxt(datesFname, delimiter=',', names=True,
            dtype=['S10', 'i8', 'S10', 'i8', 'i8', 'i8', 'S10', 'i8',
            'i8', 'i8', 'f8'])
        self.month = self.dates['mo']
        self.year = self.dates['year'] + 2000
        self.interval = self.dates['interval']
        self.intervalSession = np.where(self.interval < (self.maxTrapNights + 1),
        self.interval, self.maxTrapNights)
        self.removeDat = removeDatFX(self.nsession, self.stoat, self.session)


        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.cellX = self.covDat['x1']
        self.cellY = self.covDat['x2']
        self.eastCov = self.covDat['x1'] - min(self.covDat['x1'])
        self.northCov = self.covDat['x2'] - min(self.covDat['x2'])
        self.ncell = len(self.eastCov)

        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)

        self.xdat = np.hstack([np.expand_dims(np.ones(self.ncell),1), np.expand_dims(self.scaleEast,1),
            np.expand_dims(self.scaleNorth,1)])
        self.beta = np.matrix([[0.215], [0.06], [0.17]])
        self.mu = np.dot(self.xdat,self.beta)
        self.habValue = self.mu.flat


class GibbsData(object):
    def __init__(self, gibbsFname, summaryFname):
        self.gibbsResults = np.loadtxt(gibbsFname)
        self.npara = np.shape(self.gibbsResults)[1]
        self.ngibbs = np.shape(self.gibbsResults)[0]
        self.nDataGibbs = self.gibbsResults[:, (3 + 1) : (self.npara - 4)]
        self.rPool = self.gibbsResults[:, self.npara - 4]
        self.iPool = self.gibbsResults[:, self.npara - 3]
        self.g0Pool =  self.gibbsResults[:, self.npara - 2]
        self.sigmaPool = self.gibbsResults[:, self.npara - 1]

#        self.summaryTable = np.loadtxt(summaryFname)
        self.summaryTable = np.genfromtxt(summaryFname, names=True,
            dtype=['S10', 'f8', 'f8', 'f8', 'f8'])
        meanN = self.summaryTable['Mean']
        ll = len(meanN)
        self.meanN = meanN[4:(ll-4)]
        self.meanN[8] = 2
#        print(self.meanN)


class PlotWork(object):
    def __init__(self, gibbsdata, basicdata):

        self.mo = basicdata.month
        self.yr = basicdata.year

    def plotFX(self, basicdata):
        dates = []
        minDate = datetime.date(2008, 5, 15)
        maxDate = datetime.date(2013, 1, 30)
        for month, year in zip(self.mo, self.yr):
            date = datetime.date(int(year), int(month), 15)
            dates.append(date)
        P.figure(0)
        P.plot(dates, basicdata.removeDat, color = 'k', linewidth = 3,  marker='o', linestyle='-')
        P.xlabel('Time', fontsize = 17)
        P.ylabel('Number of captured stoats', fontsize = 17)

        P.xticks([datetime.date(2009, 1, 1), datetime.date(2010, 1, 1), datetime.date(2011, 1, 1), 
            datetime.date(2012, 1, 1), datetime.date(2013, 1, 1)], ['Jan 2009',
            'Jan 2010', 'Jan 2011', 'Jan 2012', 'Jan 2013'])

        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
            
#            ax.xaxis.set_major_locator(MaxNLocator(7))
#            tick.label.set_rotation('vertical')
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate)
        P.ylim(-10, 250)
        P.savefig("removedStoats2_I.eps", format='eps')
        P.savefig("removedStoats2_I.pdf", format='pdf')
        P.savefig("removedStoats2_I.png", format='png', dpi = 1000)
        P.show()


    def plotFX2(self, basicdata, gibbsdata):
        dates = []
        minDate = datetime.date(2008, 5, 15)
        maxDate = datetime.date(2013, 1, 30)
        for month, year in zip(self.mo, self.yr):
            date = datetime.date(int(year), int(month), 15)
            dates.append(date)

        P.figure(0)
        P.plot(dates, basicdata.removeDat, color = 'k', linewidth = 3,  marker='o', 
            label = 'Stoats Captured', linestyle='-')
        P.plot(dates, gibbsdata.meanN, 'rs', label = 'Population size', color = 'r')
        P.xlabel('Time', fontsize = 17)
        P.ylabel('Number of stoats', fontsize = 17)
        P.legend(loc='upper right')
        P.xticks([datetime.date(2009, 1, 1), datetime.date(2010, 1, 1), datetime.date(2011, 1, 1), 
            datetime.date(2012, 1, 1), datetime.date(2013, 1, 1)], ['Jan 2009',
            'Jan 2010', 'Jan 2011', 'Jan 2012', 'Jan 2013'])

        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
            
#            ax.xaxis.set_major_locator(MaxNLocator(7))
#            tick.label.set_rotation('vertical')
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xlim(minDate, maxDate)
        P.ylim(-10, 350)
        P.savefig("removedStoats_N_I.eps", format='eps')
        P.savefig("removedStoats_N_I.pdf", format='pdf')
        P.savefig("removedStoats_N_I.png", format='png', dpi = 1000)
        P.show()

    def makeAsciiFX(self, basicdata):
#        (ysize, xsize, nbands) = basicdata.mu.shape
#        print(ysize,xsize,nbands)

        maxE = np.max(basicdata.cellX)
        minE = np.min(basicdata.cellX)
        maxN = np.max(basicdata.cellY)
        minN = np.min(basicdata.cellY)

#        diagDist = distxy(minE, maxN, maxE, minN)
#        numbCell = diagDist / 500

        xrange = (maxE - minE)
        ncellX = (xrange + 500) / 500
        ncellX = int(np.ceil(ncellX))
        yrange = (maxN - minN)
        ncellY = (yrange + 500) / 500
        ncellY = int(np.ceil(ncellY))

        self.hab = np.zeros(shape = (ncellY,ncellX))
        # convert to rows and columns
        cols = (basicdata.cellX - minE) / 500
        cols = cols.round().astype(int)

        rows = (maxN - basicdata.cellY) / 500
        rows = rows.round().astype(int)

        print('rows', rows[0:20])
        print('cols', cols[0:20])
        print('shape value', np.shape(basicdata.habValue))

        self.hab[rows, cols] = basicdata.habValue

        driver = gdal.GetDriverByName('GTiff') 
        ds = driver.Create('out5.tif', ncellX, ncellY, 1, gdal.GDT_Float32) 

        band = ds.GetRasterBand(1)
        band.SetNoDataValue(0)
        band.WriteArray(self.hab)
        ds.FlushCache

        transform = [minE, 500, 0, maxN, 0, -500]
        ds.SetGeoTransform(transform)

        del ds

########            Main function
#######
def main():

    #np.seterr(all='raise')

    basicdata = BasicData('capt11.csv', 'datesBindSim.csv', 'traploc3.csv', 'covDat.csv')

    gibbsdata = GibbsData('mcmcResults_Rd_M14_R5_I.txt', 'summaryTable_RD_M14_R5_I.txt')

    plotwork = PlotWork(gibbsdata, basicdata)
    plotwork.plotFX(basicdata)
    plotwork.plotFX2(basicdata, gibbsdata)
    plotwork.makeAsciiFX(basicdata)

if __name__ == '__main__':
    main()



