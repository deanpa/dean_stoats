#!/usr/bin/env python

import os
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import datetime

class ResultsProcessing(object):
    def __init__(self, gibbsobj, stoatpath):
        """
        Class and functions to process mcmc results, and 
        produce summary table and diagnostic trace plots
        """

        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.resultsdata = np.hstack([self.gibbsobj.bgibbs,
                                    self.gibbsobj.Ngibbs,
                                    np.expand_dims(self.gibbsobj.rgibbs, 1),
                                    np.expand_dims(self.gibbsobj.ggibbs, 1),
                                    np.expand_dims(self.gibbsobj.siggibbs, 1),
                                    self.gibbsobj.vgibbs])

        self.ncov = np.shape(self.resultsdata)[1]
        self.ndat = np.shape(self.resultsdata)[0]


    @staticmethod    
    def quantileFX(a):
        """
        quantile function
        """
        return mquantiles(a, prob=[0.05, 0.5, 0.95])
    
    def makeTableFX(self):
        """
        make summary table of parameter estimates from mcmc
        """
        resultTable = np.zeros(shape = (4, 32))
        resultTable[0:3, :] = np.round(np.apply_along_axis(self.quantileFX, 0, self.resultsdata), 4)
        resultTable[3, :] = np.round(np.apply_along_axis(np.mean, 0, self.resultsdata), 4)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'N19', 'N20', 'N21', 'N22', 'N23', 'N24',
            'Repro', 'g0',
             'HR_Sigma', 'Not_Vulner_Jan', 'Not_Vulner_Jul', 'Not_Vulner_Nov']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.summaryTable = np.round(resultTable.copy(), 4)
        #return resultTable

    def plotFX(self):
        """
        plot diagnostic trace plots
        """
        cc = 0
        nfigures = np.int(np.ceil(self.ncov/6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < self.ncov:
                    P.plot(self.resultsdata[0:self.ndat, cc])
                    P.title(cc)
                cc = cc + 1
            P.show(block=lastFigure)

########            Write data to file
########
    def writeToFileFX(self, stoatpath):
        """
        write to directory a table of summary statistics of parameters
        """
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        names = ['Eastings', 'Northings', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'N19', 'N20', 'N21', 'N22', 'N23', 'N24', 
            'Repro', 'g0', 'HR_Sigma', 'Not_Vulner_Jan', 
            'Not_Vulner_Jul', 'Not_Vulner_Nov']

        structured['Names'] = names
        # path and data files of results to write to project directory
        self.summaryFile = os.path.join(stoatpath,'sumTab_res96_M11.txt')

        np.savetxt(self.summaryFile, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')



class NPredObj(object):
    def __init__(self, gibbsobj, basicdata, stoatpath):
        """
        Class and functions to plot npred, removal, and recruits
        """
        self.gibbsobj = gibbsobj
        self.basicdata = basicdata
        self.stoatpath = stoatpath
        self.NpredFX()
        print('npred', self.nPred, 'removeDat', self.basicdata.removeDat, 
            'sum Remove', np.sum(self.basicdata.removeDat))
        self.plotNpred()

    def NpredFX(self):
        """
        predicted N from population model
        """
        self.getR_Imm()
        self.nPred = np.zeros(self.basicdata.nsession)
        self.nPred[0] = self.meanPop[0]
        self.recruits = np.zeros(self.basicdata.nsession)
        for i in range(1, self.basicdata.nsession):
            Nday = self.meanPop[(i - 1)] - self.basicdata.removeDat[(i - 1)]
            Nday = np.where(Nday < 0.0, 0.0, Nday)
            self.recruits[i] = (self.rSession[i] * Nday) - Nday
            Nday = self.rSession[i] * Nday
            self.nPred[i] = Nday

    def getR_Imm(self):
        """
        get mean r and imm parameters
        """
        self.meanPop = np.mean(self.gibbsobj.Ngibbs, axis = 0)

        self.meanR = np.mean(self.gibbsobj.rgibbs, axis = 0)
        self.rSession = np.ones(self.basicdata.nsession)
        self.rSession[self.basicdata.month == 11] = self.meanR

    def getTrapSetDates(self):
        """
        get dates traps were set
        """
        self.julianZero = 0.0   # np.zeros(self.basicdata.nsession) # self.julian - self.julian[0]
        self.startDate = datetime.date(2008, 7, 17)
        self.dates = self.startDate
        for i in range(1, self.basicdata.nsession):
            self.julianZero += self.basicdata.interval[i] * 1.0
            deltaDays = self.julianZero 
            date_i = self.startDate + datetime.timedelta(days = deltaDays)
            self.dates = np.append(self.dates, date_i)


    def plotNpred(self):
        """
        plot mean and upper 95% quantile of population size for each year
        """
        self.getTrapSetDates()
        self.minDate = datetime.date(2008, 6, 1)
        self.maxDate = datetime.date(2015, 2, 28)
        f,(ax,ax2) = P.subplots(2,1, sharex=True)
        ax = P.gca()
        # plot the same data on both axes
        lns1 = ax.plot(self.dates, self.nPred, label = 'Stoat population size.', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.dates, self.recruits, 'bo', label = 'New recruits', ms = 10.0)          #, color = 'b', linewidth = 2)
        lns3 = ax.plot(self.dates, self.basicdata.removeDat, 'ro', label = 'Stoats captured', ms = 8.0)
        # combine legend items
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([-15, 235])
        ax.set_xlim(self.minDate, self.maxDate)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        # hide the spines between ax and ax2
        # axis labels
        ax.set_ylabel('Numbers', fontsize = 15)
        ax.set_xlabel('Dates', fontsize = 15)
        ax.set_xticks([datetime.date(2009, 1, 1), datetime.date(2010, 1, 1), datetime.date(2011, 1, 1),
                    datetime.date(2012, 1, 1), datetime.date(2013, 1, 1), datetime.date(2014, 1, 1),
                    datetime.date(2015, 1, 1)])
        # save plot to directory
        plotFname = os.path.join(self.stoatpath, 'N_removed_recruit.png')
        P.savefig(plotFname, format='png')
        P.show()


def main():

    # path to project directory to read in data and write results
    stoatpath = os.getenv('STOATSPROJDIR', default='.')

    inputGibbs = os.path.join(stoatpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    inputBasicdata = os.path.join(stoatpath, 'out_basicdata.pkl')
    fileobj = open(inputBasicdata, 'rb')
    basicdata = pickle.load(fileobj)
    fileobj.close()



    # run ResultsProcessing and read in data
    resultsobj = ResultsProcessing(gibbsobj, stoatpath)
    
    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
#    resultsobj.writeToFileFX(stoatpath)

    npredobj = NPredObj(gibbsobj, basicdata, stoatpath)
   
if __name__ == '__main__':
    main()


