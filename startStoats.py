#!/usr/bin/env python

import sys
import ressy97
import optparse

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--basicdata", dest="basicdata", help="Input basicdata pkl")
        p.add_option("--params", dest="params", help="Input params pkl")
        p.add_option("--stoatdata", dest="stoatdata", help="Input stoatdata pkl")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print(cmdargs.basicdata)
print(cmdargs.params)
print(cmdargs.stoatdata)
#print('sys.argv', sys.argv)

if len(sys.argv) == 1:
    # no basicdata
    ressy97.main()

else:
    #then passed the name of the pickle file on the command line
    ressy97.main(cmdargs.basicdata, cmdargs.params, cmdargs.stoatdata)



