#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#import pylab as P
from numba import autojit


def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

@autojit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat

def initialPStoatTrapCaptFX(params, basicdata, availTrapNights, location, 
    g0Param, debug = False):      # prob that stoat was capt in trap
    distToTraps = basicdata.distTrapToCell2[:, location]
    eterm = np.exp(-(distToTraps) / params.var2)           # prob stoat-trap pair
    pNoCapt = 1. - g0Param * eterm
    pNoCaptNights = pNoCapt**(availTrapNights)
    pNoCaptNights = np.where(pNoCaptNights == 1., .9999, pNoCaptNights)
    return 1 - pNoCaptNights


def NpredInitialFX(nsession, N, removeDat, rpara, it, Npred):
    for i in range(nsession)[0:nsession-1]:
        Nday = N[i] - removeDat[i]
        Nday = np.where(Nday < 0, 0, Nday)
        Nday = np.multiply(rpara[i+1],Nday) + it[i]        # it[i+1]   # imm upto previous
        Npred[i+1] = Nday
    return(Npred)

def removeDatFX(nsession, stoat, session):
    removeDat = np.arange(nsession)
    for i in range(nsession):
        removeDat[i] = np.sum(stoat[session==i])
    return(removeDat)

def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))      
    return gampdf

#def gamma_pdf(xx, shape, rate):
#    gampdf = (rate**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-rate*xx)
#    return gampdf


def lth_sPropose(params, basicdata, debug = False):
    rid = np.random.choice(basicdata.cellID, params.lth_nRand, replace = False)
    newlth = np.random.normal(params.lth[rid], .015, params.lth_nRand)
    params.lth_s[rid] =  newlth

class PrevResults(object):
    def __init__(self, prevStoatFname, basicdata):
#        self.prevStoat = np.loadtxt(prevStoatFname, dtype = ['i8', 'i8', 'i8', 'i8', 'i8'])

        self.prevStoat = np.loadtxt(prevStoatFname)

        self.prevStoatSession = self.prevStoat[:, 0]
        self.prevStoatLoc = self.prevStoat[:, 1]
#        print(self.prevStoatLoc[590:610])
        self.prevStoatPres = self.prevStoat[:, 2]
#        print(self.prevStoatPres[590:610])
        self.prevStoatRemove = self.prevStoat[:,3]
        self.prevStoatTrapID = self.prevStoat[:,4]

        self.prevN = np.zeros(basicdata.nsession, dtype = int)

        for i in range(basicdata.nsession):
            sessMask = self.prevStoatSession == i
            presSess = self.prevStoatPres[sessMask]        # pres data length of session
        
            self.prevN[i] = np.int_(np.sum(presSess))
        


class StoatData(object):
    def __init__(self, prevresults, basicdata, params, debug = False):
        """
        This is some documentation for this function it takes a number of parameters
        """
        self.stoatSession = np.int_(prevresults.prevStoatSession)
        self.stoatID = np.tile(np.arange(0, params.maxN), basicdata.nsession)  
        self.stoatLoc = np.int_(prevresults.prevStoatLoc)
        self.stoatPres = np.int_(prevresults.prevStoatPres)
        self.stoatRemove = np.int_(prevresults.prevStoatRemove)
        self.stoatTrapID = np.int_(prevresults.prevStoatTrapID)
        self.stoatPCapt = np.zeros(np.multiply(params.maxN, basicdata.nsession))
        self.stoatTrapPCapt =  np.zeros(np.multiply(params.maxN, basicdata.nsession))

        def calcPNoCaptFX(availTrapNights, eterm, g0Param, debug = False):     # prob that stoat was capt in trap
            """
            calc prob NO capt for each stoat - trap pair
            """
            pNoCapt = 1.0 - g0Param * eterm
            pNoCaptNights = pNoCapt**(availTrapNights)
            pNoCaptNights = np.where(pNoCaptNights == 1.0, 0.9999, pNoCaptNights)
            return pNoCaptNights

        def getPCapt(self, basicdata, params):
            """
            get pcapt and prob of trap capt for each stoat
            """
            for i in range(basicdata.nsession):
                sessMask = self.stoatSession == i          # session mask length of data
                presSess = self.stoatPres[sessMask]        # pres data length of session
                presSessMask = presSess == 1               # pres mask length of session
                remSess = self.stoatRemove[sessMask]       # rem data length of session
                remPresMask = remSess[presSessMask] == 1   # rem mask length of pres only in session
                remMask = remSess == 1                     # rem mask length of session

                stoatLocSess = self.stoatLoc[sessMask]     # loc data length of session
                stoatLoc = stoatLocSess[presSessMask]           # loc data length of pres only in session

                (uLoc, indxLoc) = np.unique(stoatLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                            #get indices to feedback pCapt to stoat data
                eterm = params.expTermMat[:, uLoc]                 # exp term of unique locs

                trapSessionMask = basicdata.trapSession == i
                availTrapNights = np.expand_dims(basicdata.trapNightsAvail[trapSessionMask], 1)

                pNoCaptAll = calcPNoCaptFX(availTrapNights, eterm, params.g0)
                pNoCapt = pNoCaptAll.prod(axis = 0)
                pCapt = 1.0 - pNoCapt                                   # p Capt at unique locs

                pCaptLoc = pCapt[indxLoc]                               # feedback new pCapt to stoat data
                stoatPCaptSession = self.stoatPCapt[sessMask]
                stoatPCaptSession[presSessMask] = pCaptLoc
                self.stoatPCapt[sessMask] = stoatPCaptSession 

                remIndx = indxLoc[remPresMask]                              # locations only of captured stoats
                tidSess = self.stoatTrapID[sessMask]
                remtid = tidSess[remMask]                        # trap id of captured stoats
                
                stoatTrapPNoCapt = pNoCaptAll[remtid, remIndx]          # p No Capt of captured stoats only
                stoatTrapPCaptSession = self.stoatTrapPCapt[sessMask]      # template
                stoatTrapPCaptSession[remMask] = 1.0 - stoatTrapPNoCapt # p capt fill in template
                self.stoatTrapPCapt[sessMask] = stoatTrapPCaptSession # fill in class data in case keep new params.

        getPCapt(self, basicdata, params)
        self.stoatPCapt_s = self.stoatPCapt.copy()
        self.stoatTrapPCapt_s = self.stoatTrapPCapt.copy()
        

class BasicData(object):
    def __init__(self, captFname, datesFname, trapFname, covFname, maxTrapNights = 7):
        self.maxTrapNights = maxTrapNights

        self.capt6 = np.genfromtxt(captFname,  delimiter=',', names=True, 
            dtype=['f8', 'S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'S10', 'S10', 
            'i8', 'i8', 'i8', 'S12', 'i8', 'i8', 'i8', 'f8', 'i8', 'f8', 'i8'])

        self.trap = np.genfromtxt(trapFname, delimiter=',', names=True,
            dtype=['S10', 'f8', 'f8', 'f8', 'f8'])  

        self.stoat = self.capt6['stoat']
        self.session = self.capt6['session'] - 1
        self.nsession = np.int(max(self.session + 1)) 
        self.avail = np.where(self.capt6['avail']==0.25, 0.5, self.capt6['avail'])

        self.dates = np.genfromtxt(datesFname, delimiter=',', names=True,
            dtype=['S10', 'i8', 'S10', 'i8', 'i8', 'i8', 'S10', 'i8', 
            'i8', 'i8', 'f8'])
        self.month = self.dates['mo']
        self.interval = self.dates['interval']
        self.intervalSession = np.where(self.interval < (self.maxTrapNights + 1), 
        self.interval, self.maxTrapNights)
        

        def getTrapIDFX(self):
            self.ntrap = len(self.trap['sid'])
            trapSid = self.trap['sid']
            self.trapID = np.arange(self.ntrap)
            self.trapX = self.trap['easting']
            self.trapY = self.trap['northing']
            captSid = self.capt6['sid']
            nCaptDat = len(captSid)
            self.captTrapID = np.empty(nCaptDat, dtype = int)           # trap ID in capt6 data

            for i in range(nCaptDat):
                self.captTrapID[i] = self.trapID[captSid[i] == trapSid]
                
        getTrapIDFX(self)

        def gettrapSession(self):
            sess = np.arange(self.nsession)
            self.trapSession = np.repeat(sess, self.ntrap)

        gettrapSession(self)


        def getTrapNightsFX(self):
            self.trapNightsAvail = np.zeros(self.nsession * self.ntrap)
            self.trapStoatCapt = np.zeros(self.nsession * self.ntrap)        # stoat captures by trap and session             
            for i in range(self.nsession):
                sessmask = self.trapSession == i
                captIDSession = self.captTrapID[self.session == i]   # trap ID in capt6 data
                availSession = self.avail[self.session == i]         # avail in capt6 data
                tmpTN = np.zeros(self.ntrap)
                tmptrapstoatcapt = np.zeros(self.ntrap)
                stoatSession = self.stoat[self.session == i]

                for j in range(len(captIDSession)):
                    tid = self.trapID[self.trapID == captIDSession[j]]
                    tmpTN[tid] = availSession[j] * self.intervalSession[i]
                    tmptrapstoatcapt[tid] = stoatSession[j]
                    
                self.trapNightsAvail[sessmask] = tmpTN
                self.trapStoatCapt[sessmask] = tmptrapstoatcapt
                
        getTrapNightsFX(self)

        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.cellX = self.covDat['x1']
        self.cellY = self.covDat['x2']       
        self.eastCov = self.covDat['x1'] - min(self.covDat['x1'])  
        self.northCov = self.covDat['x2'] - min(self.covDat['x2'])
        self.nw = self.covDat['cellDevNW']
        self.ndev = self.covDat['cellDevN']
        self.terrIndx = self.covDat['terrIndx']
        self.ncell = len(self.eastCov)

#        self.cellSession = np.repeat(np.arange(0, 14, 1), self.ncell)
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleNW = (self.nw - np.mean(self.nw)) / np.std(self.nw)
        self.scaleDevN = (self.ndev - np.mean(self.ndev)) / np.std(self.ndev)
        self.scaleTerrIndx = (self.terrIndx - np.mean(self.terrIndx)) / np.std(self.terrIndx)
        
        self.scaleEast2 = (self.eastCov**2 - np.mean(self.eastCov**2)) / np.std(self.eastCov**2)
        self.scaleNorth2 = (self.northCov**2 - np.mean(self.northCov**2)) / np.std(self.northCov**2)

        self.xdat = np.hstack([np.expand_dims(np.ones(self.ncell),1), np.expand_dims(self.scaleEast,1), 
            np.expand_dims(self.scaleNorth,1)])
        self.nbcov = np.shape(self.xdat)[1]

        self.removeDat = removeDatFX(self.nsession, self.stoat, self.session) 
        self.cellID = np.arange(0, self.ncell, dtype = int)
        self.nStoatInCellTemplate = np.zeros(self.ncell)

        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0
#        self.uLocList = list()
#        self.presLocList = list()            
#        self.indxLocList = list()

class Params(object):
    def __init__(self, basicdata, prevresults):
        self.sigma = 270.0
        self.sigma_s = 274.0
        self.sigma_mean = 255
        self.sigma_sd = 40
        self.sigma_search_sd = 4

        self.g0 = 0.044
        self.g0_s = 0.043   
        self.g0_alpha = 0.91    #1.16    #2.7   #4.79 #6.79
        self.g0_beta = 19.31    #24.70    #60    #101.65 #162.88

        self.g0Sd = 0.002
        self.ig =  0.55
        self.it = np.multiply(self.ig, np.divide(np.float32(basicdata.interval),365))
        self.i_s =  .57
        self.it_s = np.multiply(self.i_s, np.divide(np.float32(basicdata.interval),365))
        self.imm_shape = 3.0
        self.imm_scale = 3.0
#        self.N = np.array([419, 150, 80, 35, 138, 54, 62, 22, 4, 8, 8, 10, 49, 28, 20, 44])
#        self.N = np.array([350, 85, 52, 24, 112, 47, 53, 19, 3, 8, 7, 7, 43, 23, 7, 38])
        self.N = prevresults.prevN.copy()
        self.rg = 9.5
        self.rs = 9.6
        self.r_shape = 20   #4.0
        self.r_scale = 5.4  # 4.5  #0.88888
        self.b = np.array([.25, 0.03, 0.1])
        self.bs = np.random.normal(self.b, .01)
        self.nbcov = len(self.b)
        self.lth = np.dot(basicdata.xdat,self.b)
        self.thMultiNom = thProbFX(self.lth, debug = False)
        self.lth_s = np.dot(basicdata.xdat,self.bs)
        self.thMultiNom_s = thProbFX(self.lth_s, debug = False)

        self.rpara = np.where(basicdata.month==1,self.rg,1)
#        self.reduceRepro = .50	
#        self.rpara[4] = self.rg * self.reduceRepro
        self.rpara_s = np.where(basicdata.month==1,self.rs,1)
#        self.rpara_s[4] = self.rs * self.reduceRepro

        
        self.Npred = np.multiply(self.N,1)
        self.Npred = NpredInitialFX(basicdata.nsession, self.N, basicdata.removeDat,
            self.rpara, self.it, self.Npred)
        self.Npred_s = self.Npred.copy()
        self.maxN = 500
        self.bPrior = 0
        self.bPriorSD = np.sqrt(1000)
        self.nsample = np.array([-2, -1, 1, 2])
        self.datseq = np.arange(0, self.maxN)
        self.var2 = 2.0 * (self.sigma**2.0)
        self.var2_s = 2.0 * (self.sigma_s**2.0)
#        self.lth_nRand = np.int(basicdata.ncell / 2.0)
        self.llikTh = np.empty(basicdata.nsession)
        self.llikTh_s = np.empty(basicdata.nsession)
        self.llikR = np.empty(basicdata.nsession - 1)
        self.llikR_s = np.empty(basicdata.nsession - 1)
        self.llikImm = np.empty(basicdata.nsession - 1)
        self.llikImm_s = np.empty(basicdata.nsession - 1)
        self.llikg0Sig = np.empty(basicdata.nsession)
        self.llikg0Sig_s = np.empty(basicdata.nsession)
        self.expTermMat =  np.exp(-(basicdata.distTrapToCell2) / self.var2)
        self.expTermMat_s =  np.exp(-(basicdata.distTrapToCell2) / self.var2_s)


class MCMC(object):
    def __init__(self, params, stoatdata, basicdata):
        self.ngibbs = 50
        self.bgibbs = np.zeros([self.ngibbs, len(params.b)])
        self.rgibbs = np.zeros(self.ngibbs)
        self.Ngibbs = np.zeros([self.ngibbs, len(params.N)])
        self.igibbs = np.zeros(self.ngibbs)
        self.ggibbs = np.zeros(self.ngibbs)
        self.siggibbs = np.zeros(self.ngibbs)

        self.thinrate = 1
        self.burnin = 10
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin), 
            self.thinrate)
        self.params = params
        self.stoatdata = stoatdata
        self.basicdata = basicdata

#####################
#####################
#####################
        # path and data files of results to write to project directory
        self.stoatpath = os.getenv('STOATSPROJDIR', default='.')
        self.mcmcResultFile = os.path.join(self.stoatpath,'mcmcResults_ressy80.txt')
        self.stoatDatFile = os.path.join(self.stoatpath,'stoatData_ressy80.txt')
#####################
#####################
#####################

    def PCaptStoatFX(self, availTrapNights, eTermMat, debug = False):   # prob stoat capt in 1 of many traps
        pNoCapt = 1.0 - self.params.g0 * eTermMat
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights = np.where(pNoCaptNights == 1.0, 0.9999, pNoCaptNights)
        pNoCaptNightsTraps = pNoCaptNights.prod(axis = 0)
        return 1.0 - pNoCaptNightsTraps

    def PStoatTrapCaptFX(self, availTrapNights, eTermMat, debug = False):      # prob that stoat was capt in trap
        pNoCapt = 1.0 - self.params.g0 * eTermMat
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights = np.where(pNoCaptNights == 1.0, 0.9999, pNoCaptNights)
        return 1.0 - pNoCaptNights


    def getBetaFX(self):
        a = self.params.g0 * ((self.params.g0 * (1.0 - self.params.g0)) / self.params.g0Sd**2.0 - 1.0)
        b = (1.0 - self.params.g0) * ((self.params.g0 * (1.0 - self.params.g0)) / self.params.g0Sd**2.0 - 1.0)
        return np.random.beta(a, b, size = None)

########
########       Block of functions for updating N
########
  
    def N_stoatdata_updateFX(self):
        for i in range(self.basicdata.nsession):
                                                ####### Update N
                                                 # get proposed Ns and stoatdata_s and llik
            (Ns, sessionMask, stoatLoc_s, stoatPres_s, stoatPCapt_s, llik, llik_s,
                availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask) = self.proposeNFX(i, 
                debug = False)


                                                # pnow pnew Npred            
            presOnlyMask = self.N_PnowPnewFX(Ns, i, llik, 
                llik_s, presOnlyMask, presOnlyMask_s, stoatLoc_s, stoatPres_s, 
                stoatPCapt_s, sessionMask, debug = False)
                                                # returns stoatloc
                                                # returns stoatPres
                                                # returns stoatPCapt

    
                                                ########### update stoat location
                                                # propose new stoatloc and pcapt
            (stoatLocSession, stoatPCaptSession, stoatRemoveSession, stoatPresSession, 
                stoatTrapIDSession, stoatTrapPCaptSession, remMask, 
                nStoatCellSession) = self.updateStoatLocFX(i, 
                sessionMask, availTrapNights, presOnlyMask, debug = False) 


                                                ########## update stoats that are removed
                                                # return stoatloc and stoatPcapt
            if self.basicdata.removeDat[i] > 0:        
                (stoatRemoveSession, stoatTrapIDSession, 
                    stoatTrapPCaptSession,remMask) = self.updateStoatRemoveFX(i, sessionMask, 
                    presOnlyMask, stoatLocSession, stoatPCaptSession, 
                    stoatRemoveSession, stoatPresSession, stoatTrapIDSession, 
                    stoatTrapPCaptSession, remMask, availTrapNights, debug = False)




                                                ######### update trapid of traps that catch stoats
                                                #########
            if self.basicdata.removeDat[i] > 1:
                self.updateTrapIDFX(i, sessionMask, stoatLocSession, stoatPresSession,
                    stoatTrapIDSession, stoatTrapPCaptSession, remMask, 
                    availTrapNights, debug = False)
       

                                                 ######## calc th likelihoods by session
            self.thetaLikelihoodFX(i, stoatLocSession, presOnlyMask, sessionMask)


                                                ######## get llik for g0 and g0_s
            self.g0SigLLikFX(i, availTrapNights, stoatLocSession, stoatTrapIDSession,
                stoatPCaptSession, stoatTrapPCaptSession, sessionMask, presOnlyMask, 
                stoatRemoveSession, remMask)



#########
########  End updating N and stoatdata
########

    def proposeNFX(self, i, debug = False):    # get proposed Ns and assoc stoat data   
                                                                        # use FX in NupdateFX
        rDat = self.basicdata.removeDat[i]
        Ns = self.params.N[i] + np.random.permutation(self.params.nsample)[0]
        Ns = np.where(Ns <= 0, 1, Ns)
        Ns = np.where(Ns <= rDat, self.params.N[i] + 1, Ns)
        Ns = np.where(Ns == self.params.N[i], self.params.N[i] + 1, Ns)
        Ns = np.where(Ns >  self.params.maxN, self.params.N[i] - 1, Ns)
        Ns = np.where(Ns == self.params.N[i], self.params.N[i] - 1, Ns)
        sessionMask = self.stoatdata.stoatSession == i

        stoatRemoveSession = self.stoatdata.stoatRemove[sessionMask]
        stoatPresSession = self.stoatdata.stoatPres[sessionMask]
        stoatPCaptSession = self.stoatdata.stoatPCapt[sessionMask]
        presOnlyMask = stoatPresSession == 1                    #mask of pres only
        stoatLoc_s = self.stoatdata.stoatLoc[sessionMask]
        stoatPres_s = stoatPresSession.copy()
        stoatPCapt_s = stoatPCaptSession.copy()

        trapSessionMask = self.basicdata.trapSession == i
        availTrapNights = np.expand_dims(self.basicdata.trapNightsAvail[trapSessionMask], 1)         
                                                            # get proposed stoat data for Ns
        presOnlyMask_s = self.proposeStoatFX(Ns, i, stoatPres_s, stoatLoc_s,
            stoatPCapt_s, availTrapNights, stoatRemoveSession, 
            stoatPresSession, presOnlyMask, debug = False)

                                                            # get binomial log likelihood for N and Ns
        llik = self.NLogLikFX(self.params.N[i], rDat, stoatPCaptSession[presOnlyMask], 
            stoatRemoveSession[presOnlyMask], debug = False)        #(i > 11)
        llik_s = self.NLogLikFX(Ns, rDat, stoatPCapt_s[presOnlyMask_s], 
            stoatRemoveSession[presOnlyMask_s], debug = False)

        return (Ns, sessionMask, stoatLoc_s, stoatPres_s, stoatPCapt_s, llik, llik_s,
            availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask)



    def N_PnowPnewFX(self, Ns, i, llik, llik_s, presOnlyMask, presOnlyMask_s,
        stoatLoc_s, stoatPres_s, stoatPCapt_s, sessionMask, debug = False):

        if i == 0:
            Npred2_s = self.npredFX(i, Ns, self.params.rpara[1], self.params.it[0])     # it[1]
            pnow = llik + np.log(stats.poisson.pmf(self.params.N[1], self.params.Npred[1]))
            pnew = llik_s + np.log(stats.poisson.pmf(self.params.N[1], Npred2_s))
            self.params.Npred[0] = self.params.N[0]

        if (i > 0) and (i < (self.basicdata.nsession - 1)):
            self.params.Npred[i] = self.npredFX((i - 1), self.params.N[i - 1], 
                self.params.rpara[i], self.params.it[i-1])                                #it[i]
            Npred2_s = self.npredFX(i, Ns, self.params.rpara[i+1], self.params.it[i])         #it[i+1]  
            dpoisN1 =  np.log(stats.poisson.pmf(self.params.N[i], self.params.Npred[i]))
            dpoisN2 =  np.log(stats.poisson.pmf(self.params.N[i + 1], self.params.Npred[i + 1]))
            pnow = llik + dpoisN1 + dpoisN2

            dpoisN1_s = np.log(stats.poisson.pmf(Ns, self.params.Npred[i]))
            dpoisN2_s = np.log(stats.poisson.pmf(self.params.N[i + 1], Npred2_s))
            pnew = llik_s + dpoisN1_s + dpoisN2_s

        if i == (self.basicdata.nsession -1):
            self.params.Npred[i] = self.npredFX((i - 1), self.params.N[i - 1], 
                self.params.rpara[i], self.params.it[i-1])
            pnow = llik + np.log(stats.poisson.pmf(self.params.N[i], self.params.Npred[i]))
            pnew = llik_s + np.log(stats.poisson.pmf(Ns, self.params.Npred[i])) 
            Npred2_s = 1.0

        rValue = np.exp(pnew - pnow)        # calc importance ratio            
        zValue = np.random.uniform(0,1, size = None)

        if rValue > zValue:
            if i < (self.basicdata.nsession -1):
                self.params.Npred[i + 1] = Npred2_s

            self.params.N[i] = Ns
            self.stoatdata.stoatLoc[sessionMask] = stoatLoc_s
            self.stoatdata.stoatPres[sessionMask] = stoatPres_s
            self.stoatdata.stoatPCapt[sessionMask] = stoatPCapt_s
            presOnlyMask = presOnlyMask_s.copy()
        return presOnlyMask


    def npredFX(self, ii, nn, rr, Immigr):                        # get Npred and Npred.s for new N
        Ntmp = nn - self.basicdata.removeDat[ii]
        npredout = (rr * Ntmp) + Immigr
        return npredout
                                                                
                                                                # FX nested in proposeNFX
    def proposeStoatFX(self, Ns, i, stoatPres_s, stoatLoc_s,
        stoatPCapt_s, availTrapNights, stoatRemoveSession, 
        stoatPresSession, presOnlyMask, debug = False):
        
        if Ns > self.params.N[i]:
            potentialAddIndx = self.params.datseq[presOnlyMask == False]
            nadd = Ns - self.params.N[i]            # number stoats added
            randindx = np.random.randint(0, self.basicdata.ncell, nadd)
            add_id = np.random.choice(potentialAddIndx, nadd, replace = False)
            stoatPres_s[add_id] = 1
            stoatLoc_s[add_id] = randindx
            pCaptNew = self.PCaptStoatFX(availTrapNights, self.params.expTermMat[:,randindx])
            stoatPCapt_s[add_id] = pCaptNew
 
           
        if Ns < self.params.N[i]:
            nrem = self.params.N[i] - Ns
            removeSessionMask = stoatRemoveSession == 0
            stoatPresMask = stoatPresSession == 1
            potentialRemoveMask = stoatPresMask & removeSessionMask
            potentialRemove = self.params.datseq[potentialRemoveMask]        # select from 0:maxN
            remIndx = np.random.choice(potentialRemove, nrem, replace = False)
            stoatLoc_s[remIndx] = 0
            stoatPres_s[remIndx] = 0
            stoatPCapt_s[remIndx] = 0

        presOnlyMask_s = stoatPres_s == 1
        return presOnlyMask_s


    @staticmethod
    def NLogLikFX(n, remd, captp, sessrem, debug=False):     # FX get binomial log lik
                                                             # FX nested in ProposeNFX 
        comboTerm = gammaln(n + 1) - gammaln(n - remd + 1)
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second  
        lProb = np.sum(np.log(binProball))
        llik_term = comboTerm + lProb
        return llik_term        
#########
#########           End block of functions for updating N


#######
#######            Begin functions for updating stoatLoc
#######
    def updateStoatLocFX(self, i, sessionMask, availTrapNights,
        presOnlyMask, debug = False):

        stoatPresSession = self.stoatdata.stoatPres[sessionMask]
        stoatLocSession = self.stoatdata.stoatLoc[sessionMask]
        stoatPCaptSession = self.stoatdata.stoatPCapt[sessionMask]
        stoatRemoveSession = self.stoatdata.stoatRemove[sessionMask]
        remMask = stoatRemoveSession == 1               # mask only stoats removed
        stoatPCapt_s = stoatPCaptSession.copy()
        stoatLoc_s = stoatLocSession.copy()
        stoatTrapIDSession = self.stoatdata.stoatTrapID[sessionMask]
        stoatTrapPCaptSession = self.stoatdata.stoatTrapPCapt[sessionMask]
        
        nChangeLoc = int(np.ceil(self.params.N[i] * .20))
        potentialChangeID = self.params.datseq[presOnlyMask]
        changeID =  np.random.choice(potentialChangeID, nChangeLoc, replace = False)

        changeMask = np.in1d(self.params.datseq, changeID)
        Loc_s = np.random.randint(self.basicdata.ncell,size = nChangeLoc)
        stoatLoc_s[changeMask] = Loc_s
    
        pCaptNew = self.PCaptStoatFX(availTrapNights, self.params.expTermMat[:, Loc_s])

        stoatPCapt_s[changeMask] = pCaptNew

        Z_llik = self.NLogLikFX(1, 1, stoatPCaptSession[changeMask],
            stoatRemoveSession[changeMask], debug = False)             # llik binom capt data (z)

        Z_llik_s = self.NLogLikFX(1, 1, stoatPCapt_s[changeMask],
            stoatRemoveSession[changeMask])

        (Loc_llik, nStoatCellSession) = self.locMultiNom(stoatLocSession, presOnlyMask, 
            self.params.thMultiNom, debug = False)
        (Loc_llik_s, nStoatCellSession_s) = self.locMultiNom(stoatLoc_s, presOnlyMask, 
            self.params.thMultiNom, debug = False)

        changeRemoveMask = remMask & changeMask
        stoatTrapPCaptChangeRemove = stoatTrapPCaptSession[changeRemoveMask] 

        Loc_sChangeRemove = stoatLoc_s[changeRemoveMask]
        tid = stoatTrapIDSession[changeRemoveMask]

        etermsess = self.params.expTermMat[tid, Loc_sChangeRemove]

        tnightsavail = availTrapNights[tid]
        pCaptTrapNew = self.PStoatTrapCaptFX(tnightsavail, etermsess, debug = False)
        stoatTrapPCaptChangeRemove_s = pCaptTrapNew

        Trap_llik = np.sum(np.log(stoatTrapPCaptChangeRemove))
        Trap_llik_s = np.sum(np.log(stoatTrapPCaptChangeRemove_s))

        pnow = Z_llik + Loc_llik + Trap_llik
        pnew = Z_llik_s + Loc_llik_s + Trap_llik_s

        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0,1, size = None)

        if rValue > zValue:
            self.stoatdata.stoatLoc[sessionMask] = stoatLoc_s          # don't need to return ???
            self.stoatdata.stoatPCapt[sessionMask] = stoatPCapt_s
            stoatPCaptSession = stoatPCapt_s.copy()
            stoatLocSession = stoatLoc_s.copy()
            stoatTrapPCaptSession[changeRemoveMask] = stoatTrapPCaptChangeRemove_s
            self.stoatdata.stoatTrapPCapt[sessionMask] = stoatTrapPCaptSession
            nStoatCellSession = nStoatCellSession_s

        return (stoatLocSession, stoatPCaptSession, stoatRemoveSession, stoatPresSession, 
            stoatTrapIDSession, stoatTrapPCaptSession,remMask, nStoatCellSession)


    def locMultiNom(self, stoatLocTest, presOnlyMask, thmn, debug = False):
        """
        Calc multinom density function for habitat
        """
        nStoatCell =  np.bincount(stoatLocTest[presOnlyMask], minlength = self.basicdata.ncell)
        log_mn = multinomial_pmf(thmn, nStoatCell)
        return (log_mn, nStoatCell)
        

#######
######      End function for updating stoat location

#####
#######    Begin functions for updating stoat remove
#####
    def updateStoatRemoveFX(self, i, sessionMask,
        presOnlyMask, stoatLocSession, stoatPCaptSession, stoatRemoveSession, 
        stoatPresSession, stoatTrapIDSession, 
        stoatTrapPCaptSession, remMask, availTrapNights, debug = False):
        """
        update stoats that are captured one by one.
        """
        tmpNotRemMask = stoatRemoveSession == 0                     # mask of stoats not removed
        presNotRemMask = presOnlyMask & tmpNotRemMask               # mask of removed stoats
        nrem = self.basicdata.removeDat[i]                          # number stoats removed
        remID = self.params.datseq[remMask]                         # id indx of removed stoats
        notRemID = self.params.datseq[presNotRemMask]               # present individ not removed

        for j in range(nrem):                                       # cycle through all removed stoats, propose alternative
            currentID = remID[j]
            currentTrapID = stoatTrapIDSession[currentID]
            stoatID_s = np.random.choice(notRemID, size = None, replace = True)
            stoatLoc_s = stoatLocSession[stoatID_s]

            zLLik = np.log(stoatPCaptSession[currentID])
            zLLik_s = np.log(stoatPCaptSession[stoatID_s])

            eterm2 = self.params.expTermMat[currentTrapID, stoatLoc_s]
            tnightsavail = availTrapNights[currentTrapID]
            pCaptTrapNewStoat = self.PStoatTrapCaptFX(tnightsavail, eterm2, debug = False)
            stoatTrapPCapt_s = pCaptTrapNewStoat

            TrapLLik = np.log(stoatTrapPCaptSession[currentID])
            TrapLLik_s = np.log(stoatTrapPCapt_s) 

            pnow = zLLik + TrapLLik
            pnew = zLLik_s + TrapLLik_s
            rValue = np.exp(pnew - pnow)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
            if rValue > zValue:
                stoatRemoveSession[currentID] = 0
                stoatRemoveSession[stoatID_s] = 1
                stoatTrapIDSession[stoatID_s] = stoatTrapIDSession[currentID]
                stoatTrapIDSession[currentID] = 0

                stoatTrapPCaptSession[stoatID_s] = stoatTrapPCapt_s
                stoatTrapPCaptSession[currentID] = 0
                remID[j] = stoatID_s
                notRemID[notRemID == stoatID_s] = currentID    

            currentID = remID[j]                                      # id of current removed stoat [j]
            stoatID_s = np.random.choice(notRemID, size = None, replace = True)


        remMask = stoatRemoveSession == 1
        self.stoatdata.stoatRemove[sessionMask] = stoatRemoveSession
        self.stoatdata.stoatTrapID[sessionMask] = stoatTrapIDSession
        self.stoatdata.stoatTrapPCapt[sessionMask] = stoatTrapPCaptSession

        return (stoatRemoveSession, stoatTrapIDSession, stoatTrapPCaptSession,
            remMask)

#######
######      End function for updating stoat removal

#####
#######    Begin functions for updating trap id: 
#####
    def updateTrapIDFX(self, i, sessionMask, stoatLocSession, stoatPresSession, 
        stoatTrapIDSession, stoatTrapPCaptSession, remMask, availTrapNights,  
        debug = False):
        """
        update the trap that captures an individual stoat.
        """
        nrem = self.basicdata.removeDat[i]                          # number stoats removed
        remIndx = self.params.datseq[remMask]                         # id indx of removed stoats

        for j in range(nrem):
            stoatIndx = remIndx[j]
            indxPool = remIndx[remIndx != stoatIndx]
            stoatIndx_s = np.random.choice(indxPool, size = None, replace = True) # cycle through all removed stoats, propose alternative
            stoatLoc = stoatLocSession[stoatIndx]    # this block propose new trap for current stoat
            stoatLoc_s = stoatLocSession[stoatIndx_s]

            trapID_now = stoatTrapIDSession[stoatIndx]    # trapID[j]
            trapID_s = stoatTrapIDSession[stoatIndx_s]

            pCapt = stoatTrapPCaptSession[stoatIndx]      
            pCapt_s = stoatTrapPCaptSession[stoatIndx_s]    

            pCaptSwitch = self.PStoatTrapCaptFX(availTrapNights[trapID_s], 
                self.params.expTermMat[trapID_s, stoatLoc])

            pCaptSwitch_s = self.PStoatTrapCaptFX(availTrapNights[trapID_now],
                self.params.expTermMat[trapID_now, stoatLoc_s])

            pnow = np.log(pCapt) + np.log(pCapt_s)
            pnew = np.log(pCaptSwitch) + np.log(pCaptSwitch_s)

            rValue = np.exp(pnew - pnow)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
      
            if rValue > zValue:

                stoatTrapIDSession[stoatIndx] = trapID_s
                stoatTrapIDSession[stoatIndx_s] = trapID_now
                stoatTrapPCaptSession[stoatIndx] = pCaptSwitch
                stoatTrapPCaptSession[stoatIndx_s] = pCaptSwitch_s

        self.stoatdata.stoatTrapID[sessionMask] = stoatTrapIDSession
        self.stoatdata.stoatTrapPCapt[sessionMask] = stoatTrapPCaptSession


#######
###########         begin Beta - theta update ##########
#####
    def thetaLikelihoodFX(self, i, stoatLocSession, presOnlyMask, sessionMask):
        """
        get multinomial llik for each session
        """
        nStoatCell = np.bincount(stoatLocSession[presOnlyMask], minlength = self.basicdata.ncell)
        self.params.llikTh[i] = multinomial_pmf(self.params.thMultiNom, nStoatCell)
        self.params.llikTh_s[i] = multinomial_pmf(self.params.thMultiNom_s, nStoatCell)


    def betaUpdateFX(self):
        """
        updata betas all at same time
        """
        prior_pdf = stats.norm.logpdf(self.params.b, self.params.bPrior, self.params.bPriorSD)
        prior_pdf_s = stats.norm.logpdf(self.params.bs, self.params.bPrior, self.params.bPriorSD)

        pnow = np.sum(self.params.llikTh) + np.sum(prior_pdf)
        pnew = np.sum(self.params.llikTh_s) + np.sum(prior_pdf_s)
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.params.b = self.params.bs.copy()
            self.params.lth = self.params.lth_s.copy()
            self.params.llikTh = self.params.llikTh_s.copy()
            self.params.thMultiNom = self.params.thMultiNom_s.copy()
        self.params.bs = np.random.normal(self.params.b, .025)
        self.params.lth_s = np.dot(self.basicdata.xdat, self.params.bs)
        self.params.thMultiNom_s = thProbFX(self.params.lth_s)



#######
###########         begin r update ##########
#####
    def rLLikFX(self):
        """
        get llik for rg and rs
        i in [0, nsession - 1]
        """
        for i in range(self.basicdata.nsession - 1):
            self.params.llikR[i] = np.log(stats.poisson.pmf(self.params.N[i+1], self.params.Npred[i+1]))
            self.params.Npred_s[i + 1] = self.npredFX(i, self.params.N[i], self.params.rpara_s[i + 1],
                self.params.it[i])
            self.params.llikR_s[i] = np.log(stats.poisson.pmf(self.params.N[i+1], self.params.Npred_s[i+1]))

    def rUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.rLLikFX()
        pnow = np.sum(self.params.llikR) + np.log(gamma_pdf(self.params.rg,
            self.params.r_shape, (1.0 / self.params.r_scale)))
        pnew = np.sum(self.params.llikR_s) + np.log(gamma_pdf(self.params.rs,
            self.params.r_shape, (1.0 / self.params.r_scale)))

        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)

        if rValue > zValue:
            self.params.rg = self.params.rs
            self.params.rpara = self.params.rpara_s.copy()
            self.params.Npred = self.params.Npred_s.copy()
        self.params.rs = np.exp(np.random.normal(np.log(self.params.rg), 0.3, size = None))
        self.params.rpara_s = np.where(self.basicdata.month==1, self.params.rs, 1)

#######
###########         begin I update ##########
#####
    def immLLikFX(self):
        """
        get llik for ig and is
        i in [0, nsession - 1]
        """
        for i in range(self.basicdata.nsession - 1):
            self.params.llikImm[i] = np.log(stats.poisson.pmf(self.params.N[i+1], self.params.Npred[i+1]))
            self.params.Npred_s[i + 1] = self.npredFX(i, self.params.N[i], self.params.rpara[i + 1],
                self.params.it_s[i])
            self.params.llikImm_s[i] = np.log(stats.poisson.pmf(self.params.N[i+1], self.params.Npred_s[i+1]))


    def immUpdateFX(self):
        """
        update ig in mcmc function
        """
        self.immLLikFX()
        pnow = np.sum(self.params.llikImm) + np.log(gamma_pdf(self.params.ig,
            self.params.imm_shape, (1.0 / self.params.imm_scale)))
        pnew = np.sum(self.params.llikImm_s) + np.log(gamma_pdf(self.params.i_s,
            self.params.imm_shape, (1.0 / self.params.imm_scale)))

        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        if rValue > zValue:
            self.params.ig = self.params.i_s
            self.params.it = self.params.it_s.copy()
            self.params.Npred = self.params.Npred_s.copy()
        self.params.i_s = np.exp(np.random.normal(np.log(self.params.ig), 0.17, size = None))
        self.params.it_s = np.multiply(self.params.i_s, np.divide(np.float32(self.basicdata.interval),365.0))


        
#######
###########         begin g0 and Sigma  update ##########
#####

    def g0Sig_PStoatCaptFX(self, availTrapNights, eterm, g0Param, debug = False):     # prob that stoat was capt in trap
        pNoCapt = 1.0 - g0Param * eterm
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights = np.where(pNoCaptNights == 1.0, 0.9999, pNoCaptNights)
        return pNoCaptNights


    @staticmethod
    def g0Sig_ProbsFX(captp, sessrem, debug=False):     # FX get binomial log lik
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second  
        llik_g0Sig = np.sum(np.log(binProball))
        return llik_g0Sig         


    def g0SigLLikFX(self, i, availTrapNights, stoatLocSession, stoatTrapIDSession,
        stoatPCaptSession, stoatTrapPCaptSession, sessionMask, presOnlyMask, 
        stoatRemoveSession, remMask):
        """
        get llik for g0 and g0_s
        """
        presLoc = stoatLocSession[presOnlyMask]
        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to stoat data
        eterm_s = self.params.expTermMat_s[:, uLoc]                 # exp term of unique locs
        pNoCaptAll_s = self.g0Sig_PStoatCaptFX(availTrapNights, eterm_s, self.params.g0_s)
        pNoCapt_s = pNoCaptAll_s.prod(axis = 0)
        pCapt_s = 1.0 - pNoCapt_s                                   # p Capt at unique locs

        pCaptLoc_s = pCapt_s[indxLoc]                               # feedback new pCapt to stoat data
        stoatPCaptLoc_sSession = stoatPCaptSession.copy()
        stoatPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
        self.stoatdata.stoatPCapt_s[sessionMask] = stoatPCaptLoc_sSession #keep this in case keep new g0 and sig

                                                                    # the following gets info only for capt stoats
        remPresMask = stoatRemoveSession[presOnlyMask] == 1         # mask of removed stoats in sess i
        remIndx = indxLoc[remPresMask]                              # locations only of captured stoats
        remtid = stoatTrapIDSession[remMask]                        # trap id of captured stoats
        stoatTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured stoats only
        stoatTrapPCapt_sSession = stoatTrapPCaptSession.copy()      # template
        stoatTrapPCapt_sSession[remMask] = 1.0 - stoatTrapPNoCapt_s # p capt fill in template
        self.stoatdata.stoatTrapPCapt_s[sessionMask] = stoatTrapPCapt_sSession # fill in class data in case keep new params.


        self.params.llikg0Sig[i] = self.g0Sig_ProbsFX(stoatPCaptSession[presOnlyMask], 
            stoatRemoveSession[presOnlyMask]) 

        self.params.llikg0Sig_s[i] = self.g0Sig_ProbsFX(pCaptLoc_s,
            stoatRemoveSession[presOnlyMask])


    def g0SigUpdateFX(self):
        """
        update g0 in mcmc function
        """
        pnow = np.sum(self.params.llikg0Sig) + np.log(stats.beta.pdf(self.params.g0, 
            self.params.g0_alpha, self.params.g0_beta)) + np.log(stats.norm.pdf(self.params.sigma, 
            self.params.sigma_mean, self.params.sigma_sd))
        pnew = np.sum(self.params.llikg0Sig_s) + np.log(stats.beta.pdf(self.params.g0_s, 
            self.params.g0_alpha, self.params.g0_beta)) + np.log(stats.norm.pdf(self.params.sigma_s, 
            self.params.sigma_mean, self.params.sigma_sd))

        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)

        if rValue > zValue:
            self.params.g0 = self.params.g0_s
            self.params.sigma = self.params.sigma_s
            self.stoatdata.stoatPCapt = self.stoatdata.stoatPCapt_s.copy()
            self.stoatdata.stoatTrapPCapt = self.stoatdata.stoatTrapPCapt_s.copy()
            self.params.expTermMat = self.params.expTermMat_s.copy()
        self.params.g0_s = self.getBetaFX()
        self.params.sigma_s = np.random.normal(self.params.sigma, self.params.sigma_search_sd, size = None)
        self.params.var2_s = 2.0 * (self.params.sigma_s**2.0)
        self.params.expTermMat_s =  np.exp(-(self.basicdata.distTrapToCell2) / self.params.var2_s)    
        



########            Main mcmc function
########
    def mcmcFX(self):
        cc = 0
        for g in range(self.ngibbs * self.thinrate + self.burnin):

            self.N_stoatdata_updateFX()

            self.betaUpdateFX()

            self.rUpdateFX()

            self.immUpdateFX()

            self.g0SigUpdateFX()

            if g in self.keepseq:
                self.Ngibbs[cc] = self.params.N
                self.bgibbs[cc] = self.params.b
                self.rgibbs[cc] = self.params.rg
                self.igibbs[cc] = self.params.ig
                self.ggibbs[cc] = self.params.g0
                self.siggibbs[cc] = self.params.sigma

                cc = cc + 1
        return (self.bgibbs, self.Ngibbs, self.rgibbs, self.igibbs, self.ggibbs, 
                self.siggibbs)



########            Write data to file
########
    def writeToFileFX(self):
        self.results = np.hstack([self.bgibbs, self.Ngibbs,
            np.expand_dims(self.rgibbs, 1), np.expand_dims(self.igibbs, 1), 
            np.expand_dims(self.ggibbs, 1), np.expand_dims(self.siggibbs, 1)])
        np.savetxt(self.mcmcResultFile, self.results)
                  
    def writeStoatToFileFX(self):
        self.results = np.hstack([np.expand_dims(self.stoatdata.stoatSession, 1),
            np.expand_dims(self.stoatdata.stoatLoc, 1),
            np.expand_dims(self.stoatdata.stoatPres, 1), np.expand_dims(self.stoatdata.stoatRemove, 1),
            np.expand_dims(self.stoatdata.stoatTrapID, 1)])
        np.savetxt(self.stoatDatFile, self.results)


########            Main function
#######
def main():

    #np.seterr(all='raise')

    # path to project directory to read in data and write results
    stoatpath = os.getenv('STOATSPROJDIR', default='.')

    # paths and data to read in
    captDatFile = os.path.join(stoatpath,'capt13.csv')
    dateDatFile = os.path.join(stoatpath,'datesBind.csv')
    trapDatFile = os.path.join(stoatpath,'traploc5.csv')
    covDatFile = os.path.join(stoatpath,'covDat.csv')

    # run basicdata and read in data
    basicdata = BasicData(captDatFile, dateDatFile, trapDatFile, covDatFile)

    # set path to data for reading in previous results
    prevStoatDat = os.path.join(stoatpath,'stoatData_ressy80.txt')
        
    prevresults = PrevResults(prevStoatDat, basicdata)

    params = Params(basicdata, prevresults)

    stoatdata = StoatData(prevresults, basicdata, params, debug = False)

    mcmcobj = MCMC(params, stoatdata, basicdata)
    
    mcmcOut = mcmcobj.mcmcFX()
    
    mcmcobj.writeToFileFX()

    mcmcobj.writeStoatToFileFX()



if __name__ == '__main__':
    main()



  
  
  
  
  
