#!/usr/bin/env python

import os
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable

class ResultsProcessing(object):
    def __init__(self, resultsFname):
        self.resultsdata = np.loadtxt(resultsFname)
        #print 'results', self.resultsdata
        self.ncov = np.shape(self.resultsdata)[1]
        self.ndat = np.shape(self.resultsdata)[0]
        #print 'ncov', self.ncov

########################
########################
########################
        # path and data files of results to write to project directory
        self.stoatpath = os.getenv('STOATSPROJDIR', default='.')
        self.summaryFile = os.path.join(self.stoatpath,'summaryTable_ressy82_EN_NDev.txt')
########################
########################
########################


    @staticmethod    
    def quantileFX(a):
        return mquantiles(a, prob=[0.05, 0.5, 0.95])
    
    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, 25))
        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.resultsdata)
        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.resultsdata)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        #print resultTable
        #print aa
        #print np.shape(resultTable)

        names = ['Eastings', 'Northings', 'DevN', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'Immigration', 'g0', 'HR_Sigma']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.summaryTable = np.round(resultTable.copy(), 4)
        #return resultTable

    def plotFX(self):
        cc = 0
        nfigures = np.int(np.ceil(self.ncov/6.0))
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < self.ncov:
                    P.plot(self.resultsdata[0:self.ndat, cc])
                    P.title(cc)
                cc = cc + 1
            P.show(block=lastFigure)

########            Write data to file
########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        names = ['Eastings', 'Northings', 'DevN', 'N1', 'N2', 'N3', 'N4', 'N5',
            'N6', 'N7', 'N8', 'N9', 'N10', 'N11', 'N12', 'N13', 'N14', 'N15', 'N16', 'N17',
            'N18', 'Repro', 'Immigration', 'g0', 'HR_Sigma']

        structured['Names'] = names
#        print structured

        np.savetxt(self.summaryFile, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')


def main():

    # path to project directory to read in data and write results
    stoatpath = os.getenv('STOATSPROJDIR', default='.')

    # paths and data to read in
    resultDatFile = os.path.join(stoatpath,'mcmcResults_ressy82_EN_NDev.txt')
 
    # run ResultsProcessing and read in data
    resultsobj = ResultsProcessing(resultDatFile)
    
    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
    resultsobj.writeToFileFX()
   
    #P.subplot(2, 2, 1)
    #P.plot(mcmcobj.bgibbs[...,0])
    #print mcmcobj.bgibbs
    #P.subplot(2, 2, 2)
    #P.plot(mcmcobj.bgibbs[...,1])
    #P.subplot(2, 2, 3)
    #P.plot(mcmcobj.sgibbs[...])
    #P.show()

if __name__ == '__main__':
    main()


